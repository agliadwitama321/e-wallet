import AsyncStorage from '@react-native-async-storage/async-storage';

import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://yukie-server.herokuapp.com',
});

instance.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem('token');
    if (token) config.headers.Authorization = `Bearer ${token}`;

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
