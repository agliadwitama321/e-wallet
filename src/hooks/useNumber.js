import React, { useState, useEffect } from 'react';

const useNumber = () => {
  const [number, setNumber] = useState('');
  const [validNumber, setValidNumber] = useState('');

  useEffect(() => {
    const onlyNumber = (params) => {
      let num = /\d/g;
      num = params.match(num);
      num = num ? num.join('') : '';
      return num;
    };
    setValidNumber(onlyNumber(number));
  }, [number]);

  return [validNumber, setNumber];
};

export default useNumber;
