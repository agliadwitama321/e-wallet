import { useReducer } from 'react';

const counterReducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state.count < action.payload.max
        ? { ...state, count: state.count + 1 }
        : state;
    case 'DECREMENT':
      return state.count > action.payload.min
        ? { ...state, count: state.count - 1 }
        : state;
    default:
      return state;
  }
};

const useCounter = () => {
  const [state, dispatch] = useReducer(counterReducer, { count: 1 });
  return [state, dispatch];
};

export default useCounter;
