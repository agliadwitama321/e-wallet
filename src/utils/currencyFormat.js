const currencyFormat = (num) => {
  let number;
  if (num) {
    number = num;
  } else {
    number = 0;
  }

  return '' + number.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
};

export default currencyFormat;
