import createDataContext from './createDataContext';
import yukieApi from '../apis/yukieApi';
import { navigate } from '../navigations/RootNavigation';

const GET_WALLET = 'GET_WALLET';
const CLEAR_WALLET = 'CLEAR_WALLET';
const GET_RECEIVER = 'GET_RECEIVER';
const CLEAR_RECEIVER = 'CLEAR_RECEIVER';
const ERROR_GET_RECEIVER = 'ERROR_GET_RECEIVER';
const TRANSFER = 'TRANSFER';
const GET_HISTORY = 'GET_HISTORY';

const walletReducer = (state, action) => {
  switch (action.type) {
    case GET_WALLET:
      return {
        ...state,
        wallet: { ...state.wallet, ...action.payload.wallet },
        receiver404: null,
      };
    case CLEAR_WALLET:
      return {
        ...state,
        wallet: { accountNumber: null, balance: null, isActive: null },
        receiver: { name: null, accountNumber: null },
        receiver404: null,
        historyTransfer: { docs: [], totalPages: 0 },
        historyReceive: { docs: [], totalPages: 0 },
      };
    case GET_RECEIVER:
      return {
        ...state,
        receiver: { ...state.receiver, ...action.payload },
        receiver404: null,
      };
    case CLEAR_RECEIVER:
      return {
        ...state,
        receiver: { name: null, accountNumber: null },
        isTransferSuccess: false,
      };
    case ERROR_GET_RECEIVER:
      return {
        ...state,
        receiver404: action.payload,
        receiver: { name: null, accountNumber: null },
      };
    case TRANSFER:
      return { ...state, isTransferSuccess: action.payload };
    case GET_HISTORY:
      return {
        ...state,
        historyTransfer: { ...action.payload.historyTransfer },
        historyReceive: { ...action.payload.historyReceive },
      };
    default:
      return state;
  }
};

const getWallet = (dispatch) => async () => {
  try {
    const { data } = await yukieApi.get('/api/wallet');
    if (data.success) {
      dispatch({ type: GET_WALLET, payload: { wallet: data.wallet } });
    }
  } catch (error) {}
};

const createWallet = (dispatch) => async () => {
  try {
    const { data } = await yukieApi.post('/api/wallet/create');
    if (data.success) {
      navigate('AuthLocal');
    }
  } catch (error) {
    console.log(error.response.data.error);
  }
};

const clearWallet = (dispatch) => () => {
  dispatch({ type: CLEAR_WALLET });
};

const getReceiver =
  (dispatch) =>
  async ({ accountNumber }) => {
    try {
      const { data } = await yukieApi.post('/api/wallet/receiver', {
        accountNumber,
      });

      if (data.success) {
        dispatch({ type: GET_RECEIVER, payload: data.receiver });
      }
    } catch (error) {
      console.log(error.response.data);
      dispatch({
        type: ERROR_GET_RECEIVER,
        payload: 'Rekening tidak ditemukan',
      });
    }
  };

const clearReceiver = (dispatch) => () => {
  dispatch({ type: CLEAR_RECEIVER });
};

const transfer =
  (dispatch) =>
  async ({ accountNumber, amount, password }) => {
    try {
      const { data } = await yukieApi.post('/api/wallet/transfer', {
        accountNumber,
        amount: parseInt(amount),
        password,
      });

      if (data.success) {
        dispatch({ type: TRANSFER, payload: true });
      }
    } catch (error) {
      if (!error.response.data.success) {
        navigate('WalletSendFlow', { screen: 'WalletSendError' });
      }
    }
  };

const getHistory =
  (dispatch) =>
  async ({ countTransfer, countReceive }) => {
    try {
      const { data } = await yukieApi.post('/api/wallet/history', {
        countTransfer,
        countReceive,
      });
      if (data.success) {
        dispatch({
          type: GET_HISTORY,
          payload: {
            historyTransfer: data.transfer,
            historyReceive: data.receive,
          },
        });
      }
    } catch (error) {}
  };

export const { Context, Provider } = createDataContext(
  walletReducer,
  {
    getWallet,
    clearWallet,
    createWallet,
    getReceiver,
    clearReceiver,
    transfer,
    getHistory,
  },
  {
    wallet: { accountNumber: null, balance: null, isActive: null },
    receiver: { name: null, accountNumber: null },
    isTransferSuccess: false,
    receiver404: null,
    historyTransfer: { docs: [], totalPages: 0 },
    historyReceive: { docs: [], totalPages: 0 },
  }
);
