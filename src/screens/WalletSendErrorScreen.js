import React from 'react';
import { View } from 'react-native';

import {
  Button,
  Center,
  CenterHorizontal,
  colors,
  IconBox,
  ScreenWithHeader,
  Spacer,
  Text,
  Wrapper,
} from '../components';

export default function WalletSendErrorScreen({ navigation }) {
  return (
    <ScreenWithHeader headerText="Transfer Gagal" bgcolor={colors.white}>
      <Center>
        <View
          style={{
            backgroundColor: colors.white,
            padding: 32,
            borderRadius: 16,
            borderColor: colors.red,
            borderWidth: 1,
          }}
        >
          <CenterHorizontal>
            <IconBox
              mb={16}
              typeLibrary="materialCommunity"
              iconName="window-close"
              boxColor={colors.red}
            />
          </CenterHorizontal>
          <Text fontSize={16} fontWeight={600}>
            Password Salah
          </Text>
        </View>
      </Center>
      <Wrapper>
        <Button
          rounded={8}
          title="Kembali"
          onPress={() =>
            navigation.navigate('AccountFlow', { screen: 'Account' })
          }
        />
      </Wrapper>
      <Spacer bottom={32} />
    </ScreenWithHeader>
  );
}
