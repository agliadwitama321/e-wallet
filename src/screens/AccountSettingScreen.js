import React, { useContext, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';

import { Context as AuthContext } from '../contexts/AuthContext';
import { Context as AccountContext } from '../contexts/AccountContext';
import { Context as WalletContext } from '../contexts/WalletContext';

import {
  Screen,
  Text,
  Wrapper,
  Header,
  Spacer,
  IconBox,
  colors,
  Icon,
  List,
  ScreenWithHeader,
} from '../components';

export default function AccountSettingScreen({ navigation }) {
  const { signout } = useContext(AuthContext);
  const { clearWallet, getWallet } = useContext(WalletContext);
  const {
    state: {
      account: {
        user: { name, email },
      },
    },
    clearAccount,
    getAccount,
  } = useContext(AccountContext);

  useEffect(() => {
    const onFocus = navigation.addListener('focus', () => {
      getAccount();
      getWallet();
    });

    return () => {
      onFocus;
    };
  }, [navigation]);

  return (
    <ScreenWithHeader
      headerText="Pengaturan"
      headerTextMarginbottom={32}
      bgcolor={colors.white}
    >
      <Wrapper>
        <Text fontSize={13} fontWeight={600}>
          Kelola Profil
        </Text>
        <Spacer bottom={8} />
      </Wrapper>

      <List pv={16}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('AccountSettingsFlow', {
              screen: 'ProfileSetting',
              params: { name, email },
            });
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <IconBox
                iconName="user"
                iconSize={20}
                mr={16}
                width={35}
                height={35}
                rounded={8}
              />
              <View>
                <Text fontWeight={600}>{name}</Text>
                <Text fontSize={12} color="#999">
                  {email}
                </Text>
              </View>
            </View>
            <Icon
              name="keyboard-arrow-right"
              typeLibrary="materialIcons"
              size={20}
            />
          </View>
        </TouchableOpacity>
      </List>

      <List pv={16}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('AccountSettingsFlow', {
              screen: 'PasswordSetting',
            });
          }}
        >
          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <IconBox
                typeLibrary="materialIcons"
                iconName="lock"
                iconSize={20}
                mr={16}
                width={35}
                height={35}
                rounded={8}
              />
              <Text fontWeight={500}>Ganti Password</Text>
            </View>
            <Icon
              name="keyboard-arrow-right"
              typeLibrary="materialIcons"
              size={20}
            />
          </View>
        </TouchableOpacity>
      </List>
      <Spacer />

      <Wrapper>
        <Text fontSize={13} fontWeight={600}>
          Pengaturan Aplikasi
        </Text>
        <Spacer bottom={8} />
      </Wrapper>

      <List pv={16}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <IconBox
              typeLibrary="materialIcons"
              iconName="info"
              iconSize={20}
              mr={16}
              width={35}
              height={35}
              rounded={8}
            />
            <Text fontWeight={500}>Versi App</Text>
          </View>
          <Text>1.0.0</Text>
        </View>
      </List>

      <List pv={16}>
        <TouchableOpacity
          onPress={() => {
            signout();
            clearAccount();
            clearWallet();
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <IconBox
              typeLibrary="materialIcons"
              iconName="logout"
              iconSize={20}
              mr={16}
              width={35}
              height={35}
              rounded={8}
            />

            <Text fontWeight={500}>Keluar</Text>
          </View>
        </TouchableOpacity>
      </List>
    </ScreenWithHeader>
  );
}

const styles = StyleSheet.create({
  menuList: {
    backgroundColor: colors.white,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.primary,
    paddingVertical: 16,
  },
});
