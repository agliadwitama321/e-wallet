import React from 'react';
import { StyleSheet, View } from 'react-native';

import {
  Spacer,
  Screen,
  Text,
  Wrapper,
  Header,
  colors,
  List,
  Button,
  Icon,
  CenterHorizontal,
  InputSecure,
} from '../components';

export default function AccountSettingPasswordScreen({ navigation }) {
  return (
    <Screen full bgColor={colors.primary} staturBarColor="light">
      <Wrapper style={{ height: 100 }}>
        <CenterHorizontal>
          <Icon name="lock" size={50} color={colors.white} />
        </CenterHorizontal>
      </Wrapper>
      <View style={styles.box}>
        <Text center fontWeight={600} fontSize={24}>
          Password
        </Text>
        <Wrapper>
          <Spacer />
          <InputSecure placeholder="Password Lama" />
          <Spacer bottom={8} />
          <InputSecure placeholder="Password Baru" />
          <Spacer bottom={32} />
          <Button title="Ubah" />
          <Spacer bottom={8} />
          <Button title="Kembali" onPress={() => navigation.pop()} />
        </Wrapper>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  box: {
    flex: 1,
    backgroundColor: colors.white,
    paddingVertical: 16,
  },
});
