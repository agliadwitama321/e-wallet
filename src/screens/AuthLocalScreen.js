import React, { useEffect, useContext } from 'react';

import { Context as AuthContext } from '../contexts/AuthContext';
import { Screen } from '../components';

export default function AuthLocalScreen({ navigation }) {
  const { tryLocalSignin } = useContext(AuthContext);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      tryLocalSignin();
    });

    return () => {
      unsubscribe;
    };
  }, []);

  return <Screen full />;
}
