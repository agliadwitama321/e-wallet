import React, { useContext, useEffect, useReducer } from 'react';
import { StyleSheet, TouchableOpacity, View, FlatList } from 'react-native';

import { Context as WalletContext } from '../contexts/WalletContext';
import {
  Screen,
  Wrapper,
  Header,
  Text,
  Spacer,
  colors,
  IconBox,
  Button,
  ListHistory,
  ButtonCircle,
  List,
  Loading,
} from '../components';

import useCounter from '../hooks/useCounter';

export default function WalletHistoryScreen({ navigation }) {
  const {
    state: { historyTransfer, historyReceive },
    getHistory,
  } = useContext(WalletContext);

  const [{ count: countTransfer }, transferPage] = useCounter();
  const [{ count: countReceive }, receivePage] = useCounter();

  useEffect(() => {
    const onFocus = navigation.addListener('focus', () => {
      getHistory({ countTransfer });
    });

    return () => {
      onFocus;
    };
  }, [navigation, historyTransfer.totalPages, historyReceive.totalPages]);

  if (!historyTransfer || !historyReceive) {
    return <Loading />;
  }

  return (
    <Screen full bgColor={colors.white}>
      <Wrapper>
        <Header
          showBack
          headerText="Riwayat"
          onPress={() => navigation.pop()}
        />
        <Spacer bottom={32} />
      </Wrapper>

      <View style={styles.section}>
        <Wrapper>
          <Text fontSize={16} mb={16} fontWeight={600}>
            Uang Keluar
          </Text>
        </Wrapper>
        <FlatList
          data={historyTransfer.docs}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item._id}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('WalletHistoryDetail', { item })
                }
              >
                <ListHistory
                  date={item.createdAt}
                  messageSuccessColor={colors.primary}
                  amount={item.amount}
                  name={item.receiverName}
                  iconSize={25}
                  typeLibrary="materialIcons"
                  iconName="arrow-drop-up"
                  messageSuccess="Berhasil"
                />
              </TouchableOpacity>
            );
          }}
        />
        <Spacer bottom={8} />
        <Wrapper>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
            <ButtonCircle
              width={90}
              height={30}
              rounded={20}
              iconSize={20}
              typeLibrary="materialIcons"
              iconName="arrow-back"
              mr={16}
              onPress={() => {
                transferPage({ type: 'DECREMENT', payload: { min: 1 } });
                getHistory({ countTransfer, countReceive });
              }}
            />
            <ButtonCircle
              width={90}
              height={30}
              rounded={20}
              iconSize={20}
              typeLibrary="materialIcons"
              iconName="arrow-forward"
              onPress={() => {
                transferPage({
                  type: 'INCREMENT',
                  payload: { max: historyTransfer.totalPages },
                });

                getHistory({ countTransfer, countReceive });
              }}
            />
          </View>
        </Wrapper>
      </View>

      <View style={styles.section}>
        <Wrapper>
          <Text fontSize={16} mb={16} fontWeight={600}>
            Uang Masuk
          </Text>
        </Wrapper>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={historyReceive.docs}
          keyExtractor={(item) => item._id}
          renderItem={({ item }) => {
            return (
              <ListHistory
                date={item.createdAt}
                messageSuccessColor={colors.green}
                amount={item.amount}
                name={item.senderName}
                iconSize={30}
                typeLibrary="materialIcons"
                iconName="arrow-drop-down"
                messageSuccess="Terterima"
                iconBgcolor={colors.green}
              />
            );
          }}
        />
        <Spacer bottom={8} />

        <Wrapper>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
            <ButtonCircle
              bgColor={colors.green}
              width={90}
              height={30}
              rounded={20}
              iconSize={20}
              typeLibrary="materialIcons"
              iconName="arrow-back"
              mr={16}
              onPress={() => {
                receivePage({ type: 'DECREMENT', payload: { min: 1 } });
                getHistory({ countTransfer, countReceive });
              }}
            />
            <ButtonCircle
              bgColor={colors.green}
              width={90}
              height={30}
              rounded={20}
              iconSize={20}
              typeLibrary="materialIcons"
              iconName="arrow-forward"
              onPress={() => {
                receivePage({
                  type: 'INCREMENT',
                  payload: { max: historyReceive.totalPages },
                });

                getHistory({ countTransfer, countReceive });
              }}
            />
          </View>
        </Wrapper>
        <Spacer bottom={16} />
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  listStyle: {
    backgroundColor: colors.white,
    borderBottomColor: colors.primary,
    borderBottomWidth: 0.5,
    paddingVertical: 16,
  },
  section: {
    flex: 1,
  },
});
