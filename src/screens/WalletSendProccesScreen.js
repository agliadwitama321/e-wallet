import React, { useEffect, useState, useContext } from 'react';
import { StyleSheet } from 'react-native';
import { MotiView } from 'moti';

import { Context as WalletContext } from '../contexts/WalletContext';
import {
  Center,
  Screen,
  Text,
  Spacer,
  Wrapper,
  Header,
  colors,
  Icon,
  Button,
} from '../components';

export default function WalletSendProccesScreen({ navigation, route }) {
  const { name, accountNumber, amount, password } = route.params;
  const {
    state: { isTransferSuccess },
    transfer,
  } = useContext(WalletContext);
  const [isDone, setIsDone] = useState(false);

  useEffect(() => {
    const timeOut = setTimeout(() => {
      transfer({ accountNumber, amount, password });
      setIsDone(!isDone);
    }, 2000);

    return () => {
      timeOut;
    };
  }, []);

  return (
    <Screen full bgColor={colors.white}>
      <Wrapper>
        <Header headerText="Mengirim" />
      </Wrapper>
      <Center>
        <MotiView
          style={styles.circle}
          from={{
            scale: 0.8,
            backgroundColor: colors.primaryPressed,
            opacity: 0,
          }}
          animate={{
            scale: 1,
            backgroundColor: colors.primary,
            opacity: 1,
          }}
          transition={{ loop: true, type: 'timing', duration: 1500 }}
        >
          <Center>
            <Icon name="send" size={100} color={colors.white} />
          </Center>
        </MotiView>
        <Spacer bottom={16} />

        {isDone && isTransferSuccess ? (
          <MotiView
            from={{ translateY: -25, opacity: 0 }}
            animate={{ translateY: 0, opacity: 1 }}
            transition={{ type: 'timing' }}
          >
            <Text fontWeight={600} fontSize={18} color={colors.primary}>
              Berhasil
            </Text>
          </MotiView>
        ) : null}
      </Center>
      <Wrapper>
        {isDone && isTransferSuccess ? (
          <MotiView
            from={{ translateY: -25, opacity: 0 }}
            animate={{ translateY: 0, opacity: 1 }}
            transition={{ type: 'timing', delay: 1000 }}
          >
            <Button
              rounded={8}
              title="Lihat Bukti Transfer"
              onPress={() =>
                navigation.navigate('WalletSendProof', {
                  name,
                  accountNumber,
                  amount,
                })
              }
            />
          </MotiView>
        ) : null}
      </Wrapper>
      <Spacer bottom={32} />
    </Screen>
  );
}

const styles = StyleSheet.create({
  circle: {
    width: 200,
    height: 200,
    borderRadius: 100,
  },
});
