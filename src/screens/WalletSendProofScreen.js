import React, { useContext, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';

import { Context as WalletContext } from '../contexts/WalletContext';
import {
  colors,
  Header,
  Screen,
  Spacer,
  Wrapper,
  CardProof,
} from '../components';

const counterReducer = (state, payload) => {};

export default function WalletSendProofScreen({ navigation, route }) {
  const { name, accountNumber, amount } = route.params;
  const { clearReceiver } = useContext(WalletContext);

  useEffect(() => {
    const isblur = navigation.addListener('blur', () => {
      clearReceiver();
      navigation.navigate('AccountFlow', { screen: 'Account' });
    });

    return () => {
      isblur;
    };
  }, [navigation]);

  return (
    <Screen
      full
      style={{ backgroundColor: colors.primary }}
      staturBarColor="light"
    >
      <Wrapper>
        <Header
          headerText="Bukti Transfer"
          headerTextColor={colors.white}
          showBack
          onPress={() =>
            navigation.navigate('AccountFlow', { screen: 'Account' })
          }
        />
        <Spacer />
      </Wrapper>
      <Spacer bottom={200} />

      <View
        style={{
          backgroundColor: colors.whiteBlue,
          flex: 1,
        }}
      >
        <View style={{ position: 'absolute', top: -160, right: 32, left: 32 }}>
          <CardProof
            name={name}
            accountNumber={accountNumber}
            amount={amount}
            createdAt={Date.now()}
          />
        </View>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  imageStyle: {
    width: '100%',
  },
  cardProof: {
    backgroundColor: colors.white,
    padding: 32,
  },
});
