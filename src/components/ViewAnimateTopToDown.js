import React from 'react';
import { MotiView } from 'moti';

const ViewAnimateTopToDown = ({ children, delay = 0 }) => (
  <MotiView
    from={{ translateY: -25, opacity: 0 }}
    animate={{ translateY: 0, opacity: 1 }}
    delay={0}
  >
    {children}
  </MotiView>
);

export default ViewAnimateTopToDown;
