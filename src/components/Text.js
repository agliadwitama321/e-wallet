import React from 'react';
import { Text } from 'react-native';
import { useFonts } from 'expo-font';

import colors from './colors';

export default ({
  children,
  fontWeight,
  style,
  fontSize = 14,
  center = false,
  color = colors.grey444,
  mb = 0,
}) => {
  const [loaded] = useFonts({
    Inter200: require('../assets/fonts/Inter-ExtraLight.ttf'),
    Inter300: require('../assets/fonts/Inter-Light.ttf'),
    Inter400: require('../assets/fonts/Inter-Regular.ttf'),
    Inter500: require('../assets/fonts/Inter-Medium.ttf'),
    Inter600: require('../assets/fonts/Inter-SemiBold.ttf'),
    Inter700: require('../assets/fonts/Inter-Bold.ttf'),
  });

  let fontSeries;

  switch (fontWeight) {
    case 200:
      fontSeries = 'Inter200';
      break;
    case 300:
      fontSeries = 'Inter300';
      break;
    case 400:
      fontSeries = 'Inter400';
      break;
    case 500:
      fontSeries = 'Inter500';
      break;
    case 600:
      fontSeries = 'Inter600';
      break;
    case 700:
      fontSeries = 'Inter700';
      break;
    default:
      fontSeries = 'Inter400';
  }

  return !loaded ? null : (
    <Text
      style={[
        {
          fontFamily: fontSeries,
          fontSize: fontSize,
          textAlign: center ? 'center' : 'left',
          color: color,
          marginBottom: mb,
        },
        style,
      ]}
    >
      {children}
    </Text>
  );
};
