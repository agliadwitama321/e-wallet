import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';

import IconBox from './IconBox';
import Text from './Text';
import colors from './colors';

export default ({
  typeLibrary = 'materialIcons',
  onPress,
  iconName = 'keyboard-arrow-left',
  headerText,
  showBack = false,
  headerTextColor = colors.grey444,
}) => (
  <View style={styles.header}>
    {showBack ? (
      <TouchableOpacity onPress={onPress}>
        <IconBox
          typeLibrary={typeLibrary}
          iconName={iconName}
          width={40}
          height={40}
          rounded={50}
          iconSize={35}
          mr={16}
        />
      </TouchableOpacity>
    ) : null}
    <Text fontWeight={600} fontSize={24} color={headerTextColor}>
      {headerText}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
