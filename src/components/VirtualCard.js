import React, { useState } from 'react';

import { StyleSheet, View, TouchableOpacity } from 'react-native';
import currencyFormat from '../utils/currencyFormat';

import Text from './Text';
import Spacer from './Spacer';
import Icon from './Icon';
import colors from './colors';
import Center from './Center';

export default ({ name, accountNumber, balance, cardWidth = 300 }) => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <View style={[styles.cardVisa, { width: cardWidth }]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Text fontWeight={700} fontSize={15} color={colors.white}>
          JUNE
        </Text>
        <TouchableOpacity
          onPress={() => setIsVisible(!isVisible)}
          style={{
            backgroundColor: '#7c90fd',
            borderRadius: 50,
            width: 40,
            height: 40,
          }}
        >
          <Center>
            <Icon
              name={isVisible ? 'eye-slash' : 'eye'}
              size={25}
              color={colors.white}
            />
          </Center>
        </TouchableOpacity>
      </View>
      <Spacer />
      <Text color={colors.white} fontSize={12} fontWeight={700}>
        Saldo Aktif
      </Text>
      <Text fontSize={25} fontWeight={700} color={colors.white}>
        Rp {isVisible ? currencyFormat(balance) : '***'}
      </Text>
      <Spacer bottom={32} />

      <Icon
        typeLibrary="ion"
        name="md-hardware-chip"
        color={colors.yellow}
        size={50}
      />
      <Spacer bottom={32} />

      <Text fontSize={24} fontWeight={700} color={colors.white}>
        {accountNumber ? accountNumber : null}
      </Text>
      <Text fontSize={14} fontWeight={500} color={colors.white}>
        {name ? name : null}
      </Text>
      <View style={styles.circle} />
    </View>
  );
};

const styles = StyleSheet.create({
  cardVisa: {
    backgroundColor: colors.primary,
    padding: 32,
    borderRadius: 8,
    marginRight: 8,
    overflow: 'hidden',
  },
  circle: {
    position: 'absolute',
    width: 200,
    height: 200,
    borderRadius: 50,
    zIndex: -1,
    backgroundColor: '#711db9',
    left: -40,
  },
});
