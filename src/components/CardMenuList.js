import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';

import IconBox from './IconBox';
import Text from './Text';
import colors from './colors';

export default ({
  iconName = 'download',
  typeLibrary = 'ion',
  iconboxRounded = 0,
  text = 'Card List Item',
  onPress,
  textColor = colors.grey444,
  fontWeight = 600,
  boxColorIcon = colors.primary,
}) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.cardMenuItem}>
      <IconBox
        boxColor={boxColorIcon}
        typeLibrary={typeLibrary}
        iconName={iconName}
        width={35}
        height={35}
        iconSize={20}
        rounded={iconboxRounded}
        mr={16}
      />
      <Text fontWeight={fontWeight} color={textColor}>
        {text}
      </Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  cardMenuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.white,
    padding: 16,
    paddingHorizontal: 16,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.primary,
  },
});
