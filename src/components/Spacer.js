import React from 'react';
import { View } from 'react-native';

export default ({ bottom = 16 }) => <View style={{ marginBottom: bottom }} />;
