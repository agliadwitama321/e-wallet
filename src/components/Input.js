import React from 'react';
import { TextInput, View, StyleSheet, Pressable } from 'react-native';

import colors from './colors';
import Icon from './Icon';

export default ({
  rounded = 3,
  placeholder,
  secureTextEntry,
  iconInput,
  onPressIcon,
  IconName,
  onChangeText,
  value,
  keyboardType,
}) => {
  const [bc, setBc] = React.useState(colors.primary);
  return (
    <View
      style={[
        styles.textInputContainer,
        { borderRadius: rounded, borderColor: bc },
      ]}
    >
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        style={styles.textInput}
        placeholder={placeholder}
        secureTextEntry={secureTextEntry}
        onFocus={() => setBc(colors.primaryPressed)}
        onBlur={() => setBc(colors.primary)}
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
      />
      {iconInput ? (
        <Pressable
          onPress={onPressIcon}
          style={({ pressed }) => {
            return [
              {
                backgroundColor: pressed ? colors.white : colors.grey,
              },
              styles.inputIcon,
            ];
          }}
        >
          <Icon name={IconName} color={colors.primary} />
        </Pressable>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    height: 50,
    borderWidth: 1,
    borderColor: colors.primary,
    flexDirection: 'row',
  },
  textInput: {
    flex: 1,
    fontSize: 16,
    paddingHorizontal: 14,
  },
  inputIcon: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 14,
    borderRadius: 25,
  },
});
