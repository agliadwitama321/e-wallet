import React from 'react';
import { StyleSheet, Pressable } from 'react-native';

import Text from './Text';
import colors from './colors';

export default ({
  title,
  onPress,
  rounded = 3,
  buttonColorPressed = colors.primaryPressed,
  buttonColorNotPressed = colors.primary,
  upperCase = false,
  disabled,
}) => {
  const uppercase = (text) => {
    return upperCase ? text.toUpperCase() : text;
  };
  return (
    <Pressable
      onPress={onPress}
      disabled={disabled}
      style={({ pressed }) => {
        return [
          {
            backgroundColor: pressed
              ? buttonColorPressed
              : buttonColorNotPressed,
          },
          { borderRadius: rounded },
          styles.button,
        ];
      }}
    >
      <Text color={colors.white} center fontSize={16} fontWeight={600}>
        {uppercase(title)}
      </Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    height: 50,
    justifyContent: 'center',
  },
});
