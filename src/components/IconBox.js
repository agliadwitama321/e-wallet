import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

import Icon from './Icon';
import colors from './colors';

export default ({
  mr = 0,
  ml = 0,
  mb = 0,
  height = 50,
  width = 50,
  boxColor = colors.primary,
  iconSize = 24,
  iconName = 'send',
  iconColor = colors.white,
  rounded = 14,
  typeLibrary,
}) => (
  <View
    style={[
      styles.iconWithTouch,
      {
        backgroundColor: boxColor,
        width: width,
        height: height,
        borderRadius: rounded,
        marginRight: mr,
        marginLeft: ml,
        marginBottom: mb,
      },
    ]}
  >
    <Icon
      name={iconName}
      size={iconSize}
      color={iconColor}
      typeLibrary={typeLibrary}
    />
  </View>
);

const styles = StyleSheet.create({
  iconWithTouch: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
