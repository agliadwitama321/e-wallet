import React from 'react';
import { View, StyleSheet } from 'react-native';

import colors from './colors';
import IconBox from './IconBox';
import Text from './Text';

export default ({ text }) => (
  <View style={styles.cardTextError}>
    <View style={styles.card}>
      <IconBox
        boxColor={colors.white}
        typeLibrary="ion"
        iconName="alert"
        width={30}
        iconColor={colors.red}
        height={30}
        iconSize={20}
        rounded={20}
        mr={8}
      />
      <Text fontSize={14} color={colors.white} fontWeight={400}>
        {text}
      </Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  cardTextError: {
    alignItems: 'center',
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.red,
    paddingVertical: 16,
    paddingHorizontal: 16,
    borderRadius: 8,
  },
});
