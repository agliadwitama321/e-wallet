import React from 'react';

import Input from './Input';

export default ({ value, onChangeText, placeholder }) => {
  const [showPassword, setShowPassword] = React.useState(false);

  return (
    <Input
      rounded={8}
      placeholder={placeholder}
      onPressIcon={() => setShowPassword(!showPassword)}
      IconName={showPassword ? 'eye-slash' : 'eye'}
      iconInput={true}
      value={value}
      onChangeText={onChangeText}
      secureTextEntry={showPassword ? false : true}
    />
  );
};
