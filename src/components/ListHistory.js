import React from 'react';
import { View, StyleSheet } from 'react-native';
import moment from 'moment';

import currencyFormat from '../utils/currencyFormat';
import Text from './Text';
import Wrapper from './Wrapper';
import IconBox from './IconBox';
import colors from './colors';

const ListHistory = ({
  name,
  amount,
  date,
  iconName,
  iconColor,
  iconBgcolor,
  typeLibrary,
  messageSuccess,
  messageSuccessColor,
  iconSize,
}) => {
  return (
    <View style={styles.listStyle}>
      <Wrapper>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <IconBox
              iconName={iconName}
              typeLibrary={typeLibrary}
              width={30}
              height={30}
              iconSize={iconSize}
              boxColor={iconBgcolor}
              iconColor={iconColor}
              mr={16}
            />
            <View>
              <Text fontSize={12} fontWeight={600}>
                {name}
              </Text>
              <Text fontSize={11}>{moment(date).format('LL')}</Text>
            </View>
          </View>
          <View>
            <Text fontSize={12} fontWeight={600}>
              Rp{currencyFormat(amount)}
            </Text>
            <Text
              color={messageSuccessColor}
              style={{ textAlign: 'right' }}
              fontSize={11}
            >
              {messageSuccess}
            </Text>
          </View>
        </View>
      </Wrapper>
    </View>
  );
};

const styles = StyleSheet.create({
  listStyle: {
    backgroundColor: colors.white,
    borderBottomColor: '#ddd',
    borderBottomWidth: 0.5,
    paddingVertical: 12,
  },
});

export default ListHistory;
