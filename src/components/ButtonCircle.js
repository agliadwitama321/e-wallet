import React from 'react';
import { TouchableOpacity } from 'react-native';

import IconBox from './IconBox';

const ButtonCircle = ({
  onPress,
  iconName,
  typeLibrary,
  bgColor,
  mr,
  ml,
  width,
  height,
  rounded,
  iconSize,
}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <IconBox
        width={width}
        height={height}
        rounded={rounded}
        typeLibrary={typeLibrary}
        iconSize={iconSize}
        iconName={iconName}
        boxColor={bgColor}
        ml={ml}
        mr={mr}
      />
    </TouchableOpacity>
  );
};

export default ButtonCircle;
